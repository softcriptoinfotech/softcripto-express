# v2.2.0
- Route decorators Get, Put, Delete, Patch, Post, Controller, Middleware added.

# v2.1.0
Session and Flash added
Mongoose Database connection

# v2.0.0
Conversion to typescript

# 2020-06-07

- Basic Express Setup with **bodyParser, cookieParser, methodOverride, bodyParser.json()**
- added `hbs` as template engine
- Basic error handling for 4xx and any system error (5xx)