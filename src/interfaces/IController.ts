import { IRouteParams } from "./IRouter";

export interface IController {
    children: Array<IRouteParams>
}