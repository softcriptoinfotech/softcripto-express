import "reflect-metadata";
import express, { Router } from "express";
import morgan from "morgan";
import bodyParser from 'body-parser';
import cookieParser from "cookie-parser";
import methodOverride from 'method-override';
import { IServerConfiguration, IErrorParams } from "./interfaces";
import { AppRouter } from "./AppRouter";

/**
 * Class to create Node server
 * 
 * Create a file and paste
 * ```
 * import { Server } from "@softcripto/express";
 * export = Server.create({...});
 * ```
 */
export class Server {
    app: express.Express;
    constructor() {
        this.app = express();
    }
    setEssentialMiddlewares() {
        this.app.use(morgan('dev'));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(cookieParser());
        this.app.use(methodOverride());
    }
    /**
     * Static method to create server
     */
    public static create(options: IServerConfiguration) {
        const server = new Server();
        server.setEssentialMiddlewares();

        // Set template engine and public directory
        server.setViews(options.views);
        server.setPublic(options.public);

        if (options.beforeRouteInjection) {
            options.beforeRouteInjection(server.app);
        }
        // Mount Application Route
        server.mountRouter();

        if (options.afterRouteInjection) {
            options.afterRouteInjection(server.app);
        }
        server.setErrorMiddlewares();
        return server.app;
    }
    /**
     * Mount Router to Application
     */
    mountRouter() {
        AppRouter.forEach(item => {
            /// this equal to
            /// app.use('/users', requires('./users.routes.js'));
            /// where ./users.routes.js declares all sub-route for /users
            this.app.use(item.baseURL, item.routerObject);
        })
    }
    /**
     * Set error middleware
     */
    setErrorMiddlewares() {
        this._set404();
        this._set422();
        this._renderErrorMiddleware();
    }
    /**
     * Sets express template file folder and template engine
     */
    setViews(viewsDirPath: string) {
        this.app.set('views', viewsDirPath);
        this.app.set('view engine', 'hbs');
    }
    /**
     * Sets public folder for css, image, fonts and js
     */
    setPublic(publicDirPath: string) {
        this.app.use(express.static(publicDirPath));
    }
    /**
     * Sets 404 middleware to express
     */
    private _set404() {
        this.app.use((req, res, next) => {
            let error: IErrorParams = {
                message: 'Not Found: Requested service does not exists',
                status: 404
            }
            next(error);
        });
    }
    /**
     * Sets Validation Error thrown from Mongoosejs
     */
    private _set422() {
        this.app.use((
            err: IErrorParams,
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            if (err.name === 'ValidationError') {
                err.status = 422;
            }
            next(err);
        });
    }
    /**
     * Renders error
     */
    private _renderErrorMiddleware() {
        this.app.use((
            err: IErrorParams,
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            if (!err.status) err.status = 500;
            res.status(err.status as number);
            //@todo Log error
            let error: IErrorParams = {
                status: err.name,
                message: err.message
            };
            if (err.status == 422) {
                error.errors = err.errors;
            }
            res.json(error); //@todo based on request type, if api request return json else html
        });
    }
}