import { Server } from "../index";
import http from 'http';
import debug from 'debug';

// Load controllers
import "./RootController";
import "./PostController";

// Express Setup
const app = Server.create({
    views: 'views',
    public: 'public'
});

// Run server
const server = http.createServer(app);
server.listen(4000);
server.on('error', function (err) {
    console.log(err);
    process.exit(1);
});
server.on('listening', function () {
    console.log(server.address());
    debug('boilerplate:server')('Listening');
});